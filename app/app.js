(function(){
	'use strict';

	angular.module('podcasting', [ 'ngRoute','podcasting-main','templates' ])
	  .config(function ($routeProvider) {
	    $routeProvider
	      .otherwise({
	        redirectTo: '/'
	      });
	  });
	  
})();